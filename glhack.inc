
include windows.inc
include kernel32.inc
include user32.inc
include wsock32.inc
include opengl32.inc
include strtovk.inc

includelib kernel32.lib
includelib user32.lib
includelib wsock32.lib
includelib strtovklib.lib

HackIAT			PROTO	:LPVOID,:LPSTR,:LPSTR,:DWORD
RemoteExec		PROTO	:HANDLE,:HANDLE,:DWORD,:DWORD,:DWORD
RemoteExecOT	PROTO	:HANDLE,:HANDLE,:DWORD,:DWORD
RemoteExecNT	PROTO	:HANDLE,:HANDLE,:DWORD,:DWORD,:LPDWORD
CheckFileCRC	PROTO	:LPSTR
CRC32_GenTable	PROTO
CRC32			PROTO	:DWORD,:DWORD
CheckBNE		PROTO

; %%%%%%%%%%%% change this for release builds :) %%%%%%%%%%%%
DEBUG			equ 0
FK_LOOP			equ 0	; use loop to get kernel base
NOCVARVALIDATE	equ 0	; allow all cvar values - could be detected server side
NOKICK			equ 1	; no damage etc. kickangles
NOROXTRAILS		equ 1	; no rocket trails at all
FASTAUTH		equ 1	; faster auth code
TIMEAUTH		equ 0	; show how long auth took
SHOWSTUFF		equ 1	; show stufftext
PROTECT_CHDEC	equ 0	; protect against chap buffer overflow in CHAP_Decode
NOMUTEXCHECK	equ 1

; ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
literal MACRO quoted_text:VARARG
	LOCAL local_text
	DATA segment
		align 4
		local_text db quoted_text, 0
		align 4
	DATA ends
	EXITM <local_text>
ENDM

SADD MACRO quoted_text:VARARG
	EXITM <ADDR literal(quoted_text)>
ENDM

CSTR MACRO quoted_text:VARARG
	EXITM <offset literal(quoted_text)>
ENDM

fixup MACRO fname:REQ,back:=<4>
	f&fname equ dword ptr $ - back
endm

; gets program base address offset and puts it in reg
getdelta MACRO reg:REQ
	LOCAL x
	call x
x:	pop reg
	sub reg,offset x
endm

; makes a jmp [0] labelled "th".thname
jmptab MACRO thname:REQ
	LOCAL blah
	th&thname:
	blah db 0ffh, 25h, 0, 0, 0, 0
endm

; adds function offset to base and puts in named pointer
fntab MACRO fnname:REQ
	lea edx,[ebx+(x&fnname - HookCode)]
	mov hpx&fnname&,edx
endm

fillthunk MACRO fnname:REQ
	LOCAL x
	call x
	db '&fnname',0
x:	push ebx
	call GetProcAddress
	mov [edi+esi*4],eax
	inc esi
endm

hookfn MACRO fnname:REQ
	LOCAL z
	push ebx
	call z
	db '&fnname',0
z:	call thlstrcmp
	test eax,eax
	lea eax,[edi+x&fnname]
	jz hookret
endm

if DEBUG
	codebp MACRO
		db 0cch
	endm
else
	codebp MACRO
	endm
endif


; ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
.const
; wallhack modes
WH_ENTITY		equ 01h		; wallhack GL_TRIANGLE_FAN and _STRIP
WH_TRANS		equ 02h		; alpha blended walls
WH_HALFENTITY	equ 03h		; just wallhack GL_TRIANGLE_STRIP
WH_WIRE			equ 04h		; GL_POLYGON_MODE -> GL_LINE

; crc32 of q2ace elements
CRC32_Q2ACE_EXE	equ 0a7738fc7h
CRC32_Q2ACE_GL	equ  999f4e75h
CRC32_Q2CHAP	equ  3769c41bh

; q2ace entrypoint
Q2ACE_EP		equ 0043cb7ch

; q2ace return addresses
; N.B. qgl___ is the SECOND variable filled by GetProcAddress in q2 source
RET_B_PLAYER	equ 30055404h
RET_V_PLAYER	equ 300554ceh ; not used

; variable addresses in q2ace
CON_ORMASK		equ 004f1C90h
;CURRENTENTITY	equ 300b623ch
;SHADELIGHT		equ 30116910h
CMD_ARGC		equ 00459D20h
CMD_ARGV		equ 00459D24h

NA_IP			equ 00AF1540h  ; not used
IP_ZBIRN		equ 0bbdca5c1h ; not used

; function addresses in q2ace
FN_CONNECT			equ 0040a210h
FN_DISCONNECT		equ 0040a680h
FN_RECONNECT		equ 0040a6d0h
FN_SCREENSHOT		equ 3005b8c0h
FN_SCREENSHOTJPG	equ 3005b620h
FN_Z_TAGMALLOC		equ 0041b680h
FN_Z_FREE			equ 0041b5c0h
FN_COPYSTRING		equ 0041b490h
FN_GLOBALSAY		equ 004103f0h
FN_CBUF_EXECUTE		equ 00416cd0h
FN_CBUF_ADDTEXT		equ 00416b60h

; patch addresses
; force jmp to stop model resize
P0	equ 30057EFCh ; 75 -> EB (jnz -> jmp)
; force return 1 for CL_IsModelVisible
P1	equ 004027B0h ; 81 EC EC 00 -> 6A 01 58 C3
P1o equ 00ecec81h
P1p equ 0c358016ah
; another visibility check
P2	equ 00402A40h
P2o equ 00ecec81h
P2p equ 0c358016ah
; force return 0 for CHAP_CRC
P3  equ 30005B8Ah
P3b equ 30004EFBh
P3c equ 56h			; push esi
P3o equ 5328EC83h	; sub esp, 28h / push ebx
P3p equ 90C3C033h	; xor eax, eax / ret / nop
; disable rocket trails (jnz -> jmp after if(effects & EF_ROCKET))
P4	equ 00403E70h
P4o	equ 74h
P4p	equ 0EBh
; allow "naughty" cvars
P5	equ 0041D530h
P5o equ 0400EC81h
P5p equ 0C340C033h	; xor eax,eax / inc eax / ret
; no more kick angles
P6	equ 0040468Ch
P6o equ 1024548Bh	; mov edx, [esp+10h]
P6p	equ 909026EBh	; jmp ($ + 28h) / nop / nop

; for VirtualAlloc to allocate memory above 80000000h
; which is accessible by all processes
VA_SHARED			equ 08000000h	; (from ForceLibrary 1.4B by y0da)

; some of the quake2 entity flags
RF_FULLBRIGHT		equ 00000008h
RF_TRANSLUCENT		equ 00000020h
RF_GLOW				equ 00000200h
RF_SHELL_RED		equ 00000400h
RF_SHELL_GREEN		equ 00000800h
RF_SHELL_BLUE		equ 00001000h
RF_SHELL_DOUBLE		equ 00010000h
RF_SHELL_HALF_DAM	equ 00020000h
; normal shell flags combined
RF_SHELL_WHITE		equ 00001C00h
; all shell flags combined
RF_SHELL_ALL		equ 00031C00h

; ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒

.data?

;me32 MODULEENTRY32 <?>
;align 8
SINF STARTUPINFO <?>
align 8
PINF PROCESS_INFORMATION <?>
align 8
CTXT CONTEXT <?>
align 8
OSVI OSVERSIONINFO <?>
align 8

lpCRCTable db 1024 dup(?)
lpRandPool db 20h dup(?)

align 8
hFile dd ?
szModPath dd ?
szCurDir dd ?
dwRW	dd ?
bTmp label byte
dwRet	dd ?
dw3dfx	dd ?
bIs9x	dd ?
dwK32	dd ?

; ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
