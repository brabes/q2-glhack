.686
.model flat, stdcall  ;32 bit memory model
option casemap :none  ;case sensitive

include glhack.inc

; ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒

.data
	; this is a "call 3, ret 4"
	lpREStub	db 0e8h
				dd 3
				db 0c2h, 4, 0

	; this is a "call 2, jmp eip"
	lpREOTStub	db 0e8h
				dd 2
				db 0ebh, 0feh

	szAppName db 'q2ace.exe',0
	APPNAME_LEN equ $ - szAppName - 1
	szAppDll db 'q2ace_gl.dll',0
	szWsock32 db 'wsock32.dll',0
	szUser32 db 'user32.dll',0
	szOpenGL32 db 'opengl32.dll',0
	sz3dfxgl db '3dfxgl.dll',0
	szq2chap db 'q2chap.dll',0
	szGLHIni db '\glhack.ini',0
	szGLHack db 'glhack',0
	szHexFmt db '%08x',0
	szNULStr equ byte ptr ($-1)

; ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒

.code

include hookcode.asm

; resort to APIs in case of an exception
fk_seh:
	mov eax,1
	mov ecx,[esp+ 4 ]
	assume ecx:ptr EXCEPTION_RECORD
	mov edx,[esp+0Ch]
	assume edx:ptr CONTEXT
	cmp dword ptr [ecx].ExceptionCode,0C0000005h
	jnz @F
	mov [edx].regEip,offset @safe_place
	sub eax,1
	assume ecx:nothing
	assume edx:nothing
@@:	ret

start:
	align 16
	;lock mov [eax+ebx*2+1],eax
	inc eax
	align 16

	xor eax,eax
	jnc @F
	int 20h
@@:

if FK_LOOP

	mov edx,[esp]

	assume fs:nothing
	push offset fk_seh
	push fs:[eax]
	mov fs:[eax],esp

	; get kernel image base with no apis
	xor dx,dx
@@:	sub edx,1000h
	cmp word ptr [edx],'ZM'
	jnz @B
	jmp @F
@safe_place:
	invoke GetModuleHandle,SADD("kernel32.dll")
	mov edx,eax
	xor eax,eax
@@:	pop fs:[eax]
	pop eax

else

	; use TEB/TIB
	assume fs:nothing
	push offset fk_seh
	push fs:[eax]
	mov fs:[eax],esp
	mov eax,fs:[30h]	; eax <- PEB
	mov edx,0B8h
	mov ecx,[eax+30h]
	test eax,eax
	jns @K1
	mov ebx,[eax+34h]
	test ecx,ecx
	jnz @K2
@K1:mov eax,[eax+0Ch]
	sub edx,0B0h
	mov eax,[eax+1Ch]
	mov ebx,[eax]
@K2:mov edx,dword ptr [ebx+edx]
	jmp @K3
@safe_place:
	invoke GetModuleHandle,SADD("kernel32.dll")
	mov edx,eax
@K3:xor eax,eax
	pop fs:[eax]
	pop eax

endif

	; kernel base ends up in edx
	mov ebx,edx
	test edx,edx
	sets al
	movzx eax,al
	mov bIs9x,eax
	mov dwK32,ebx

	xor eax,eax
	mov al,sizeof(STARTUPINFO)
	mov SINF.cb,eax

	xor esi,esi
	invoke GetCommandLine
	mov edi,eax
	mov edx,eax				; save start of cmd line
	or ecx,-1
	and al,0
	repnz scasb
	mov edi,edx				; edi -> cmd line
	not ecx					; ecx -> command line length
	mov al,20h				; space
	cmp byte ptr [edi],22h
	jnz @F
	add al,2				; if it's a quote, skip the leading one
	add edi,1
@@:	; find our space or quote mark
	repnz scasb
	; now edi is the end of the appname i.e. the start of the args
	; check if we stopped because ecx = 0
	jnz nocmdline
	; if we looked for a space, go back one so we're on the space
	shr al,2
	cmc
	sbb edi,esi				; esi is 0
	invoke lstrlen,edi
	; allocate memory for appname and command line
	lea eax,[APPNAME_LEN+eax]
	invoke LocalAlloc,LPTR,eax
	mov esi,eax
	invoke lstrcpy,esi,ADDR szAppName
	invoke lstrcat,esi,edi
nocmdline:
	mov edi,esi

	; check for "break 'n' enter"
	invoke GetModuleHandle,NULL
	call CheckBNE
	jnc noint3
	push offset delta1
	ret
	db 0e8h
delta1:
	assume fs:nothing
	xor eax,eax
	push eax
	and eax,ebx
	pop fs:[eax]
	cmp eax,ebx
	jnz crash
	db 0c7h
crash:
	push eax
	ret
noint3:

ifndef NOMUTEXCHECK
	; do mutex check (ebx = kernel32 image base from ^^^)
	call @F
	db 'CreateMutexA',0
@@:	push ebx
	call GetProcAddress
	; if the function is below kernel32 image base then something's dodgy!
	cmp eax,ebx
	jb justexit

	; check for bpx CreateMutexA
	movzx edx,byte ptr [eax]
	sub edx,0CCh
	jz justexit				; exit if bpx CreateMutexA
	sub edx,1
	jz justexit

	push offset szMutex		; mutex name
	push 1					; flag for inital ownership
	cdq						; edx -> 0
	push edx				; security attributes
	push offset aftercrap3
	jmp eax
;	call eax
	db 0e8h
aftercrap3:

	call @F
	db 'GetLastError',0
@@:	push ebx
	call GetProcAddress
	; check if < kernel32 image base
	cmp eax,ebx
	jb justexit
	; check bpx GetLastError
	mov dl,[eax]
	cmp dl,0cch
	jz justexit

	call eax
	cmp eax,ERROR_ALREADY_EXISTS
	jz mutexok
	rdtsc
	push eax
	mov eax,esp
	push 4
	push eax
	call CRC32
	shr ax,2
	movsx eax,ax
	mov [esp],eax
	push offset sneakyexit
	jmp Sleep
	db 0ffh, 25h
sneakyexit:
	call ExitProcess
mutexok:
endif

	invoke CRC32_GenTable

	push ebx	; save kernel module handle
	push edi	; save command line
	invoke LocalAlloc,LPTR,MAX_PATH
	mov ebx,eax								; ebx -> current directory
	invoke GetCurrentDirectory,MAX_PATH,eax
	invoke LocalAlloc,LPTR,MAX_PATH
	mov edi,eax
	invoke lstrcpy,edi,ebx
	invoke lstrcat,edi,ADDR szGLHIni		; edi -> full ini path

	mov ebp,offset szGLHack
	invoke LocalAlloc,LPTR,MAX_PATH
	mov esi,eax
	; esi -> requested start directory
	invoke GetPrivateProfileString,ebp,SADD("startdir"),ebx,esi,MAX_PATH,edi
	invoke SetCurrentDirectory,esi
	test eax,eax
	push CSTR("Bad startdir")
	jz msgexit
	add esp,4
	mov szCurDir,esi
	mov esi,offset szNULStr
	invoke GetPrivateProfileInt,ebp,SADD("3dfx"),0,edi
	mov dw3dfx,eax
	; get wallhack toggle key
	invoke GetPrivateProfileString,ebp,SADD("whkey"),esi,ebx,MAX_PATH,edi
	invoke strtovk,ebx
	mov dwHackKey,eax
	; get alpha mode toggle key
	invoke GetPrivateProfileString,ebp,SADD("alphamode"),esi,ebx,MAX_PATH,edi
	invoke strtovk,ebx
	mov dwAlphaKey,eax
	; get alpha up key
	invoke GetPrivateProfileString,ebp,SADD("alphaup"),esi,ebx,MAX_PATH,edi
	invoke strtovk,ebx
	mov dwAlphaUpKey,eax
	; get alpha down key
	invoke GetPrivateProfileString,ebp,SADD("alphadn"),esi,ebx,MAX_PATH,edi
	invoke strtovk,ebx
	mov dwAlphaDnKey,eax
	; get fullbright key
	invoke GetPrivateProfileString,ebp,SADD("fullbright"),esi,ebx,MAX_PATH,edi
	invoke strtovk,ebx
	mov dwFbKey,eax
	; get speedhack key
	invoke GetPrivateProfileString,ebp,SADD("speedhack"),esi,ebx,MAX_PATH,edi
	invoke strtovk,ebx
	mov dwShKey,eax
	; get speedhack factor
	invoke GetPrivateProfileInt,ebp,SADD("shfactor"),0,edi
	mov dwShVal,eax
	; check for no rocket trails
	invoke GetPrivateProfileInt,ebp,SADD("norockettrails"),0,edi
	test eax,eax
	jz @F
	mov bRoxPatch,P4p
@@:	; check for nokick
	invoke GetPrivateProfileInt,ebp,SADD("nokick"),1,edi
	test eax,eax
	jz @F
	mov bKickPatch,P6p
@@:	invoke LocalFree,ebx
	pop edi

	; generate a pool of random stuff to crc
	mov ebx,offset lpRandPool
	rdtsc
	xor [ebx+18h],eax
	xor [ebx+18h+4],edx
	invoke GetTickCount
	mov ecx,eax
	mov eax,[ebx+18h+4]
	ror eax,cl
	mov [ebx+18h+4],eax
	invoke CRC32,ebx,20h
	mov ecx,eax
	and ecx,0ffh
	shl ecx,8
	xor eax,ecx		; fold last byte over
	push eax
	mov ebx,offset hszNewConnect
	invoke wsprintf,ebx,ADDR szHexFmt,eax
	mov byte ptr [ebx+6],0			; 6 digit hex number => 16777216 combinations
	invoke GetTickCount
	mov ecx,eax
	pop eax
	xor eax,-1
	ror eax,cl
	xchg al,ah
	mov ebx,offset hszNewCvar
	invoke wsprintf,ebx,ADDR szHexFmt,eax
	mov byte ptr [ebx+6],0
	pop ebx

	; crc32 check q2ace modules to ensure this is version 1.19
	; crc table already generated
	push CSTR("Q2ACE version mismatch")
	invoke CheckFileCRC,ADDR szAppName
	db 0b8h
	cmp eax,CRC32_Q2ACE_EXE
	jnz msgexit
	invoke CheckFileCRC,ADDR szAppDll
	db 0e8h
	stc
	sbb eax,CRC32_Q2ACE_GL
	add eax,1
	jnz msgexit
	invoke CheckFileCRC,ADDR szChap
	db 069h
	xor eax,CRC32_Q2CHAP
	jnz msgexit
	add esp,4

	invoke LocalFree,esi

	; command line in edi from long ago :) ^^^
	invoke CreateProcess,ADDR szAppName,edi,NULL,NULL,FALSE,CREATE_SUSPENDED,NULL,szCurDir,ADDR SINF,ADDR PINF
	test eax,eax
	push CSTR("Start Q2ACE failed")
	jz msgexit
	add esp,4

	invoke LocalFree,edi		; LocalFree doesn't mind if ptr = NULL

	; fill in our thunks
	; ebx = kernel32 module handle already
	; ****** THESE MUST BE IN THE RIGHT ORDER! ******
	mov edi,offset HookCode
	xor esi,esi

	fillthunk LoadLibraryA
	fillthunk GetProcAddress
	fillthunk GetLastError
	fillthunk LocalAlloc
	fillthunk LocalFree
	fillthunk lstrlen
	fillthunk lstrcmpi
	fillthunk lstrcmp
	fillthunk VirtualProtect
	fillthunk CreateThread
	fillthunk GetTickCount

	; load 3dfxgl or opengl32 dll
	mov eax,dw3dfx
	mov edx,offset szOpenGL32
	xchg eax,ecx
	jecxz @F
	mov edx,offset sz3dfxgl
@@:	invoke LoadLibrary,edx
	test eax,eax
	jz justexit
	mov ebx,eax
	fillthunk glBegin
	fillthunk glEnd
	fillthunk glIsEnabled
	fillthunk glEnable
	fillthunk glDisable
	fillthunk glColor4f
	fillthunk glVertex3fv
	fillthunk glClearColor
	fillthunk glClear
	fillthunk wglSwapBuffers
	fillthunk glReadBuffer
	fillthunk glReadPixels
	fillthunk glGetFloatv
	fillthunk glBlendFunc

	invoke GetModuleHandle,ADDR szUser32
	mov ebx,eax
	fillthunk GetAsyncKeyState
	fillthunk wsprintfA

	invoke LoadLibrary,ADDR szWsock32
	mov ebx,eax
	fillthunk gethostbyname

	; allocate space for hook code (nt/2k/xp)
	invoke VirtualAllocEx,PINF.hProcess,NULL,HookCodeLen,MEM_COMMIT,PAGE_EXECUTE_READWRITE
	test eax,eax
	jnz allocdok
	; did it fail because VirtualAllocEx isn't present?
	invoke GetLastError
	cmp eax,ERROR_CALL_NOT_IMPLEMENTED
	push CSTR("Memory allocation error!")
	jnz justexit	; if not then exit
	; otherwise allocated shared memory (9x/me)
	invoke VirtualAlloc,NULL,HookCodeLen,MEM_COMMIT or VA_SHARED,PAGE_EXECUTE_READWRITE
allocdok:
	mov ebx,eax		; save address of our code in process

	; build jmp table
	db 6ah, thunk_count
	mov edi,offset jmp_table + 2
	pop ecx
@@:	mov [edi],eax
	add edi,6
	add eax,4
	loop @B

	; perform fixups
	jmp @F
@fixups:
	dd f0000,f0001,f0002,f0003,f0004,f0005,f0006,f0007
	dd f0100
	dd f0200,f0201,f0202
	dd f0300,f0301
	dd f0400,f0401,f0402,f0403,f0404
	dd f0500,f0501
	dd fH00, fH01, fH02
	dd fFB, fFC, fFD
	dd fF002, fF003
if FASTAUTH
	dd fF004, fF005
endif
@@:
	db 6ah, (($ - offset @fixups)/4)	; force push byte
	mov edi,(offset @fixups - 4)
	pop ecx
@@:	mov eax,[edi+ecx*4]
	add [eax],ebx
	loop @B

	mov eax,WriteProcessMemory
	cmp byte ptr [eax],0CCh
	jz $+4

	; write hook code in
	invoke WriteProcessMemory,PINF.hProcess,ebx,ADDR HookCode,HookCodeLen,ADDR dwRW

	; hook the hook in
	db 6ah, InjCodeLen
	push offset InjCodeStart
	push PINF.dwProcessId
	push PINF.hThread
	push PINF.hProcess
	call RemoteExec

	; resume the q2ace thread
	invoke ResumeThread,PINF.hThread

	invoke CloseHandle,PINF.hProcess
	invoke CloseHandle,PINF.hThread
justexit:
	invoke ExitProcess,0
msgexit:
	mov eax,[esp]
	push 0
	push MB_OK or MB_ICONSTOP
	push CSTR("Error")
	push eax
	push 0
	push ExitProcess
	jmp MessageBox
	int 3				; debugger trap
	align 16

; ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒

include supportfns.asm

end start
