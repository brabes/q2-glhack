
; ħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħ

; checks for 0CCh (int 3) on program entrypoint. Sets CF if found.
; expects base address in eax!
; rvatooffs(x) === x - (sec.virtoffs - sec.rawoffs)
CheckBNE proc uses ebx esi edi

	mov ebx,eax
	invoke LocalAlloc,LPTR,MAX_PATH
	mov edi,eax
	invoke GetModuleFileName,ebx,edi,MAX_PATH
	invoke CreateFile,edi,GENERIC_READ,FILE_SHARE_READ,NULL,OPEN_EXISTING,0,NULL
	inc eax
	jnz bne_delta
	db 0e9h
bne_delta:
	dec eax
	push eax
	invoke GetFileSize,eax,NULL
	push eax
	invoke LocalAlloc,LPTR,eax
	mov ebx,eax
	pop ecx
	mov eax,[esp]
	invoke ReadFile,eax,ebx,ecx,ADDR dwRW,NULL
	call CloseHandle
	mov edx,ebx
	cmp word ptr [edx],'ZM'
	jnz bne_found
	assume edx:ptr IMAGE_DOS_HEADER
	add edx,[edx].e_lfanew
	assume edx:ptr IMAGE_NT_HEADERS
	cmp [edx].Signature,IMAGE_NT_SIGNATURE
	jnz bne_found
	push [edx].OptionalHeader.AddressOfEntryPoint	; push entrypoint RVA
	xor ecx,ecx
	mov cx,[edx].FileHeader.NumberOfSections
	test ecx,ecx
	jz bne_found									; no sections
	xor eax,eax
	mov ax,[edx].FileHeader.SizeOfOptionalHeader
	assume edx:nothing
	; move past file header and optional header
	lea edx,[edx+18h+eax]							; edx -> section table
	; search object table for section containing this rva
	assume edx:ptr IMAGE_SECTION_HEADER
	pop esi								; esi -> entrypoint rva
next:
	mov eax,[edx].VirtualAddress
	cmp esi,eax
	jb skipsec							; if esi < sec_va, skip sec
	add eax,[edx].Misc.VirtualSize
	cmp esi,eax							; if esi < (sec_va + vsize), found
	jb found
skipsec:
	lea edx,[edx+28h]					; go to next section
	dec ecx
	jnz next
	jmp bne_found
	db 0c7h
found:
	; calculate file offset from rva
	mov eax,[edx].VirtualAddress
	sub eax,[edx].PointerToRawData
	sub esi,eax							; esi -> raw entrypoint offset
	assume edx:nothing
	; check offset
	cmp byte ptr [ebx+esi],0cch
	jz bne_found
	invoke LocalFree,ebx
	invoke LocalFree,szModPath
	clc
	ret
	db 0ffh
bne_found:
	assume edx:nothing
	invoke LocalFree,szModPath
	stc
	ret

CheckBNE endp

; ħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħ

; checks os version and calls relevant version
; some anti-debug from http://daemon.anticrack.de/
RemoteExec proc hProc:HANDLE,hThread:HANDLE,dwPID:DWORD,lpCode:DWORD,cbCodeLen:DWORD

	mov OSVI.dwOSVersionInfoSize,sizeof(OSVERSIONINFO)
	invoke GetVersionEx,ADDR OSVI
	mov eax,OSVI.dwPlatformId
	dec eax
	jnz re_nt		; VER_PLATFORM_WIN32_WINDOWS is 1, NT is 2
	; check if we're being debugged
	mov eax,fs:[20h]		; TIB debug context
	test eax,eax
	jnz (re_nt - 1)
	push cbCodeLen
	push offset InjCodeStart
	push hThread
	push hProc
	push offset re_done		; was call/jmp
	jmp RemoteExecOT
	db 9ah
re_nt:
	; check if we're being debugged
	mov eax,fs:[30h]			; pointer to PEB
	movzx eax,byte ptr [eax+2h]	; check for usermode debugger
	test eax,eax
	jnz $+1
	invoke RemoteExecNT,hProc,hThread,ADDR InjCodeStart,cbCodeLen,ADDR dwRet
re_done:
	ret

RemoteExec endp

; ħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħ

RemoteExecOT proc uses ebx esi edi hProc:HANDLE,hThread:HANDLE,lpCode:DWORD,cbCodeLen:DWORD

	LOCAL dwW:DWORD, bRet:DWORD
	invoke EnterCriticalSection,ADDR CSEC
	mov edi,cbCodeLen
	lea edi,[edi+7]
	and bRet,0
	; allocate 7 + cbCodeLen bytes of shared memory (no VirtualAllocEx on 9x)
	invoke VirtualAlloc,NULL,edi,MEM_COMMIT or VA_SHARED,PAGE_EXECUTE_READWRITE
	mov ebx,eax
	lea esi,dwW
	; write the stub in
	invoke WriteProcessMemory,hProc,ebx,ADDR lpREOTStub,7,esi
	lea eax,[ebx+7]
	; write the code to be called in (after the stub space)
	invoke WriteProcessMemory,hProc,eax,lpCode,cbCodeLen,esi

	; use stub to make process run our code (no CreateRemoteThread on 9x)

;	; suspend the main thread
;	invoke SuspendThread,hThread
	; get control regs (eip, ss, cs etc.) and int regs (eax ebx..)
	mov CTXT.ContextFlags,CONTEXT_CONTROL or CONTEXT_INTEGER
	invoke GetThreadContext,hThread,ADDR CTXT
	; save old eip in esi and change eip to address of our code
	mov esi,CTXT.regEip
	mov CTXT.regEip,ebx
	invoke SetThreadContext,hThread,ADDR CTXT
	; resume the thread, it will now run the injected code
	invoke ResumeThread,hThread
	; wait for the code to do its stuff
	invoke Sleep,300
	; now suspend the thread again in order to restore the program
	invoke SuspendThread,hThread
	mov CTXT.regEip,esi
	invoke SetThreadContext,hThread,ADDR CTXT
;	invoke ResumeThread,hThread
	; free the shared memory with stub and code in
	invoke VirtualFree,ebx,edi,MEM_DECOMMIT
	invoke LeaveCriticalSection,ADDR CSEC
	ret

RemoteExecOT endp

; ħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħ

RemoteExecNT proc uses ebx esi edi hProc:HANDLE,hThread:HANDLE,lpCode:DWORD,cbCodeLen:DWORD,pdwRet:LPDWORD

	LOCAL dwW:DWORD, bRet:DWORD

	invoke EnterCriticalSection,ADDR CSEC
	mov edi,cbCodeLen
	xor eax,eax
	add edi,8
	mov bRet,eax
	; allocate cbCodeLen + 8 bytes in the process
	invoke VirtualAllocEx,hProc,NULL,edi,MEM_COMMIT,PAGE_EXECUTE_READWRITE
	mov ebx,eax
	lea esi,dwW
	; write the code to call our code in
	invoke WriteProcessMemory,hProc,ebx,ADDR lpREStub,8,esi
	; write the code to be called in
	lea ecx,[ebx+8]
	invoke WriteProcessMemory,hProc,ecx,lpCode,cbCodeLen,esi
	; flush the process's instruction cache
	invoke FlushInstructionCache,hProc,NULL,0
	; make the process create a thread running our code
	lea esi,dwW
	invoke CreateRemoteThread,hProc,NULL,0,ebx,NULL,0,esi
	xchg eax,ecx
	jecxz cleanup
	; wait for the thread to finish (max 500ms)
	mov esi,ecx
	invoke WaitForSingleObject,esi,500
	cmp eax,WAIT_OBJECT_0
	jnz cleanup4
	mov eax,pdwRet
	xchg eax,ecx
	jecxz @F
	invoke GetExitCodeThread,esi,ecx
@@:	inc bRet
cleanup1:
	invoke CloseHandle,esi
cleanup:
	invoke VirtualFreeEx,hProc,ebx,edi,MEM_DECOMMIT
	invoke LeaveCriticalSection,ADDR CSEC
	mov eax,bRet
	ret
cleanup3:
	invoke TerminateProcess,hProc,0
	jmp cleanup
cleanup4:
	invoke TerminateThread,ecx,0
	invoke TerminateProcess,hProc,0
	jmp cleanup1

RemoteExecNT endp

; ħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħ

; returns to 1 byte after the 'real' return address
CheckFileCRC proc uses esi ebx szFile:LPSTR

	invoke CreateFile,szFile,GENERIC_READ,0,NULL,OPEN_EXISTING,0,NULL
	mov esi,eax
	inc eax
	jz @F
	invoke GetFileSize,esi,NULL
	push eax					; push cbLen
	invoke LocalAlloc,LPTR,eax
	mov ebx,eax
	mov ecx,[esp]				; leave cblen on stack for crc32
	push eax					; push lpData
	invoke ReadFile,esi,eax,ecx,ADDR dwRW,NULL
	invoke CloseHandle,esi
	inc dword ptr [ebp+4]
	call CRC32
	mov esi,eax
	invoke LocalFree,ebx
	mov eax,esi
@@:	ret

CheckFileCRC endp

; ħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħ

OPTION PROLOGUE:NONE
OPTION EPILOGUE:NONE
align 16
CRC32_GenTable proc

	push edi
	push esi
	push ebx
	; generate crc table
	db 3Eh						; make the code up to lstart
	xor ecx,ecx					; take 16-bytes
	mov edi,offset lpCRCTable
	mov esi,0EDB88320h
lstart:							; ^^ to align this label
	mov edx,8
	mov eax,ecx

lsubloop:
	xor ebx,ebx
	shr eax,1		; bottom bit -> cf
	cmovc ebx,esi	; xor with the polynomial if odd
	xor eax,ebx
	sub edx,1
	jnz lsubloop

	mov [edi+ecx*4],eax
	add cl,1
	jnz lstart		; i.e. 256 iterations
	pop ebx
	pop esi
	pop edi
	ret

CRC32_GenTable endp
OPTION PROLOGUE:PROLOGUEDEF
OPTION EPILOGUE:EPILOGUEDEF

; ħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħħ

CRC32 proc uses ebx esi edi lpData:DWORD,cbLen:DWORD

	mov edi,offset lpCRCTable
	mov ecx,cbLen
	; crc = crc_table[((int)crc ^ (*buf++)) & 0xff] ^ (crc >> 8);
	or eax,-1				; start with 0xffffffff
	mov esi,lpData
	align 16
lcstart:
	xor ebx,ebx
	mov bl,[esi]
	add esi,1
	xor bl,al				; ebx = (crc ^ *buf) & 0xff
	mov ebx,[edi+ebx*4]		; ebx = crctable[ ebx ]
	shr eax,8
	xor eax,ebx				; eax = (eax >> 8) ^ ebx
	sub ecx,1
	jnz lcstart
	xor eax,-1				; flip the crc bits back again
	ret

CRC32 endp
