GetProcAddr PROC USES esi edi ebx, dwDllBase:DWORD, szApi:LPSTR
	mov esi,dwDllBase
	cmp word ptr [esi],IMAGE_DOS_SIGNATURE
	jnz @@BadExit
	add esi,[esi+03Ch]
	cmp dword ptr [esi],IMAGE_NT_SIGNATURE
	jnz @@BadExit

	; get the string length of the target Api
	mov edi,szApi
	mov ecx,MAX_API_STRING_LENGTH
	xor al,al
	repnz scasb
	mov ecx,edi
	sub ecx,szApi			; ECX -> Api string length
	
	; trace the export table
	mov edx,[esi+078h]		; edx -> export table
	add edx,dwDllBase
	assume edx:PTR IMAGE_EXPORT_DIRECTORY
	mov ebx,[edx].AddressOfNames	; ebx -> AddressOfNames array
	add ebx,dwDllBase
	xor eax,eax						; eax -> AddressOfNames index
	.repeat
		mov edi,[ebx]
		add edi,dwDllBase
		mov esi,szApi
		push ecx					; save api string length
		repz cmpsb
		.if zero?
			add esp,4
			.break
		.endif
		pop ecx
		add ebx,4
		inc eax
	.until eax == [edx].NumberOfNames

	; did we find something?
	cmp eax,[edx].NumberOfNames
	jz @@BadExit

	; find the corresponding Ordinal
	mov esi,[edx].AddressOfNameOrdinals
	add esi,dwDllBase
	push edx						; save export table pointer
	mov ebx,2
	xor edx,edx
	mul ebx
	pop edx
	add eax,esi
	xor ecx,ecx
	mov cx,[eax]					; ecx -> api ordinal
	
	; get the address of the api
	mov edi,[edx].AddressOfFunctions
	xor edx,edx
	mov ebx,4
	mov eax,ecx
	mul ebx
	add eax,dwDllBase
	add eax,edi
	mov edx,eax	; storm1x - save eat addr
	mov eax,[eax]
	add eax,dwDllBase
	jmp @@ExitProc

@@BadExit:
	xor eax,eax
@@ExitProc:
	assume edx:nothing
	ret
GetProcAddr endp
