
HookCode:

include imptable.asm

; strings
szChap db 'q2chap.dll',0
szFormat db '%s%s%02d%s%02d%s',0
szNull db 'null',0
szModelsOK db 'Models OK',0
szCustomOK db ' Custom Models OK',0
szhGetRefAPI db 'GetRefAPI',0
szhGetChapAPI db 'GetChapAPI',0

; real fn pointers
rGRA dd 0
rGCA dd 0

; variables
fltWallInc real4 0.01
fltWallTrans real4 1.0
fltCurAlpha real4 0.0
fltMille real4 0.001
dwAlphaMode dd GL_ONE
dwHackKey dd 0
dwAlphaKey dd 0
dwAlphaUpKey dd 0
dwAlphaDnKey dd 0
dwFbKey dd 0
dwShKey dd 0
dwShVal dd 0
dwDrawMode dd 0
bDrawing dd 0
bbefore_dt dd 0
bwallhack dd 0
bFb dd 0
qScreenshot dd 0
dwRetAddr dd 0
dwRetTmp dd 0
pCHAP_UniCrypt dd 0
pCHAP_Stuff dd 0
if PROTECT_CHDEC
pCHAP_Decode dd 0
endif
pCom_Printf dd 0041a230h
v0 dd 0
v1 dd 0
v2 dd 0
v3 dd 0

; for ring0
;pIDT df 0
;align 4
; for adding new commands
pCmd_AddCommand dd 004176e0h
pCmd_RemoveCommand dd 004177b0h
pCvar_Get dd 0041d460h
pR_Init dd 0
bCmdsDone dd 0
cvTargServ dd 0
cvFakeServ dd 0
cvTimeScale dd 0
pSSCmd dd 0
hszScreenshot db 'screenshot',0
hszScreenshotjpg db 'screenshotjpg',0
;hszCmdlist db 'cmdlist',0
hszNewConnect db 12 dup(0)
hszNewCvar db 12 dup(0)
hszFormat db 0dh,0ah,' >> your connect cvar is     : "%s" << ',0ah,0ah,0
hszCvarFormat db     ' ** your fake server cvar is : "%s" ** ',0ah,0ah,0
hszRemember db 0ah, '        %% Remember that the fake server cvar must be %%',0ah
			db		'        %% an  IP address and must always include the %%',0ah
			db		'        %% port number, e.g 92.40.74.284:27910 rather %%',0ah
			db		'        %% than stupid.q2ace.q2adminserver.com or you %%',0ah
			db		'        %% will fail p_way tests and be kicked by q2a %%',0ah,0ah,0
ifdef SHOWSTUFF
hszStuffFmt db 'stufftext: %s',0
endif
hszNULStr db 0
hszOldConnect db 'connect',0

bRoxPatch db P4o
bKickPatch dd P6o

; *** BEGIN glBegin ***
xglBegin:
	xor eax,eax
	add eax,1
	mov ds:(bDrawing - HookCode),eax
fixup 0000
	mov eax,[esp]
	mov ds:(dwRetAddr - HookCode),eax
fixup 0001
	mov eax,[esp+4]						; draw mode
	mov ds:(dwDrawMode - HookCode),eax	; save draw mode for sky removal
fixup 0002
	mov ecx,ds:(bFb-HookCode)
fixup 0003
	test ecx,ecx
	jz @td
	push eax			; save draw mode
	cmp eax,GL_POLYGON
	jnz @F
	push GL_TEXTURE_2D
	call thglDisable
@@:	cmp eax,GL_QUADS
	jnz @F
	push GL_TEXTURE_2D
	call thglEnable
@@: pop eax
@td:
	mov ecx,ds:(bwallhack - HookCode)
fixup 0004
	test ecx,ecx
	jz thglBegin				; don't wallhack if not wanted

	; wallhacks
	sub ecx,1
	jnz wh_trans
wh_entity:
	cmp dword ptr [esp],RET_B_PLAYER
	jnz notplayer
tex:
	; comment GL_TRIANGLE_STRIP for that half-entity wallhack
	.if eax == GL_TRIANGLE_FAN || eax == GL_TRIANGLE_STRIP
		push GL_DEPTH_TEST
		call thglDisable
	.endif
	jmp thglBegin
wh_trans:
	cmp eax,GL_POLYGON
	jnz wht_notwall
	push ebx
	mov ebx,ds:(fltWallTrans - HookCode)
fixup 0005
	push GL_DEPTH_TEST
	call thglDisable
	push GL_BLEND
	call thglEnable
	push ds:(dwAlphaMode - HookCode)
fixup 0006
	push GL_SRC_ALPHA
	call thglBlendFunc
	sub esp,10h
	push esp
	push GL_CURRENT_COLOR
	call thglGetFloatv
	mov [esp+12],ebx
	call thglColor4f		; cleans the stack
	pop ebx
	jmp thglBegin
	; make sure non-walls aren't wrongly blended
wht_notwall:
	mov eax,[esp+4]
	cmp eax,GL_TRIANGLE_FAN
	jz wht_fix
	cmp eax,GL_TRIANGLE_STRIP
	jnz thglBegin
wht_fix:
	push GL_BLEND
	cmp dword ptr ds:(fltCurAlpha-HookCode),3f800000h
fixup 0007,8
	jnz wht_blend
	call thglDisable
	jmp thglBegin
wht_blend:
	call thglEnable
	push GL_ONE_MINUS_SRC_ALPHA
	push GL_SRC_ALPHA
	call thglBlendFunc
	jmp thglBegin
	; end wallhacks
notplayer:
	push GL_DEPTH_TEST
	call thglEnable
	jmp thglBegin
; *** END glBegin ***

xglEnd:
	xor eax,eax
	mov ds:(bDrawing - HookCode),eax
fixup 0100
	jmp thglEnd

; ******************************************************************
; q2ace checks for a wallhack by the following:
;
; a = glIsEnabled(GL_DEPTH_TEST);
; glBegin(mode);
; b = glIsEnabled(GL_DEPTH_TEST);
; if(a == b) cheating = 1;
;
; so this routine always returns the opposite of what it was before
; glBegin, after glBegin by hooking glBegin and glEnd too :)
; ******************************************************************
; if(!drawing) { save answer } else { return !saved_ans }
xglIsEnabled:
	push [esp+4]
	call thglIsEnabled
	mov ecx,eax							; ecx = real answer
	mov eax,ds:(bDrawing - HookCode)	; check if we're drawing
fixup 0200
	test eax,eax
	xchg eax,ecx
	jz notdrawing
	mov eax,ds:(bbefore_dt - HookCode)
fixup 0201
	xor eax,1							; opposite of last real answer
	jmp drawing
notdrawing:
	xchg eax,ecx
	mov ds:(bbefore_dt - HookCode),eax	; save real answer and return it
fixup 0202
drawing:
	ret 4
; *** END glIsEnabled ***

; *** BEGIN glColor4f ***
xglColor4f:
	mov ecx,[esp+10h]
	mov ds:(fltCurAlpha - HookCode),ecx
fixup 0300
	mov ecx,3f800000h
	cmp [esp+0ch],ecx
	jnz @F
	cmp [esp+10h],ecx
	jnz @F
	; special case for power shield
	xor ecx,ecx
	mov ds:(fltCurAlpha - HookCode),ecx
fixup 0301
@@:	jmp thglColor4f
; *** END glColor4f ***

; *** BEGIN glVertex3fv ***
xglVertex3fv:
;	mov ecx,CURRENTENTITY
;	mov ecx,[ecx]
;	test dword ptr [ecx+44h],RF_TRANSLUCENT
;	jnz thglVertex3fv
	mov eax,ds:(qScreenshot - HookCode)
fixup 0400
	test eax,eax
	jnz no_lm
	mov eax,ds:(dwDrawMode - HookCode)
fixup 0401
	cmp eax,GL_QUADS
	jnz notsky
	mov ecx,ds:(bwallhack - HookCode)
fixup 0402
	cmp ecx,1
	jbe notsky		; wallhack mode was 1 or off
	ret 4			; just return if quads (it means it's the sky)
notsky:
	cmp eax,GL_TRIANGLE_FAN
	jz @F
	cmp eax,GL_TRIANGLE_STRIP
	jnz no_lm
@@:	cmp dword ptr ds:(dwRetAddr-HookCode),RET_B_PLAYER
fixup 0403,8
	jnz no_lm
;	cmp dword ptr [esp],RET_V_PLAYER
;	jnz no_lm
	; don't lambert things that are meant to be transparent
	mov eax,3f800000h		; 1.0
	cmp dword ptr ds:(fltCurAlpha-HookCode),eax
fixup 0404
	jnz no_lm
	push eax
	push eax
	push eax
	push eax
	call thglColor4f
no_lm:
	jmp thglVertex3fv
; *** END glVertex3fv ***

xglReadPixels:
	push GL_FRONT
	call thglReadBuffer
	jmp thglReadPixels

; *** BEGIN SCREENSHOT HACK ***
gss:
	xor eax,eax
	mov ds:(pSSCmd-HookCode),ecx
fixup 0500
	add eax,1
	mov ds:(qScreenshot-HookCode),eax
fixup 0501
xchap_test:
xfakecmdlist:					; reuse some code
	ret

xscreenshot:
	mov ecx,FN_SCREENSHOT
	jmp gss

xscreenshotjpg:
	mov ecx,FN_SCREENSHOTJPG
	jmp gss
; *** END SCREENSHOT HACK ***

xdiscon:
	call @F
	db 0ah
	db '[glhack] Admin stuffed fake connect to check for glhack',0ah
	db '         a normal q2ace client would disconnect at this',0ah
	db '         point so glhack will disconnect you now.',0ah
	db 0ah
	db 0
@@: call [ebx+pCom_Printf]
	add esp,8
	mov eax,FN_DISCONNECT
	call eax
	pop ebx
	ret

; get round q2admin 1.31
xconnect:
	push ebx
	getdelta ebx
	mov eax,ds:[CMD_ARGC]
	sub eax,1
	jnz @F	; check for args
	pop ebx
	ret
@@:	mov ecx,ds:[(CMD_ARGV+4)]
	; scan for @ or %, which indicate a phony hostname
@@:	mov al,[ecx]
	add ecx,1
	cmp al,'@'
	jz xdiscon
	cmp al,'%'
	jz xdiscon
	test al,al
	jnz @B
	push esi
	push edi
	; check cvar
	mov eax,[ebx+cvTargServ]
	mov eax,[eax+4]				; cvTargServ->string
	mov cl,[eax]
	test cl,cl
	jz @nocvar
	push eax
	; free first arg
	mov esi,(CMD_ARGV+4)
	push [esi]
	mov eax,FN_Z_FREE
	call eax
	pop ecx
	; copy cvar string
	mov eax,FN_COPYSTRING
	call eax
	pop ecx
	; place as first arg
	mov [esi],eax
	mov esi,[ebx+cvFakeServ]
	mov esi,[esi+4]				; cvFakeServ->string
	mov al,[esi]
	test al,al
	jz @nocvar
	pop edi
	mov eax,FN_CONNECT
	pop esi
	pop ebx
	jmp eax
	int 20h
@nocvar:
	call @F
	db '[glhack] You must set both cvars!',0ah,0
@@:	call [ebx+pCom_Printf]
	pop eax
	pop edi
	pop esi
	pop ebx
	ret

; *** BEGIN wglSwapBuffers ***
; have to call wglSwapBuffers _then_ glClear
xwglSwapBuffers:
	push ebx
	getdelta ebx
	lea edx,[ebx+qScreenshot]
	mov eax,[edx]
	test eax,eax
	jz noss
	sub eax,1
	jnz @F
	xor ecx,ecx
	mov [ebx+bwallhack],ecx
	inc dword ptr [edx]
	jmp noclear
@@:	sub eax,1
	jnz @F
	inc dword ptr [edx]
	jmp noclear
@@:	push edx
	call [ebx+pSSCmd]
	pop edx
	xor eax,eax
	mov [edx],eax
	jmp noclear
noss:
	lea edx,[ebx+bCmdsDone]
	mov eax,[edx]
	test eax,eax
	jnz cmds_done
	inc dword ptr [edx]
	; get timescale cvar for speedhack
	push 0
	call @F
	db 0
@@:	call @F
	db 'timescale',0
@@:	call [ebx+pCvar_Get]
	add esp,8					; only add 8, leave 4 for VirtualProtect
	mov [ebx+cvTimeScale],eax
	; patch model resize
	; FIXME: could be detected by checking page access
	push esp
	push PAGE_EXECUTE_READWRITE
	push 59000h
	push 400000h
	call thVirtualProtect
	push esp
	push PAGE_EXECUTE_READWRITE
	push 4C000h
	push 30050000h
	call thVirtualProtect
	pop ecx
	mov byte ptr ds:[P0],0ebh		; patch model resize
;	mov eax,P1
;	mov dword ptr [eax],P1p			; patch anti-wallhack
;	mov eax,P2
;	mov dword ptr [eax],P2p
	; replace connect
	lea eax,[ebx+hszOldConnect]
	push eax
	call [ebx+pCmd_RemoveCommand]
	lea eax,[ebx+xconnect]
	pop edx
	push eax
	push edx
	call [ebx+pCmd_AddCommand]
	add esp,8
	mov byte ptr ds:[CON_ORMASK],80h	; con.ormask = 0x80
	; add connect cvar
	push 0
	lea eax,[ebx+hszNULStr]
	push eax
	lea eax,[ebx+hszNewConnect]
	push eax
	lea eax,[ebx+hszFormat]
	push eax
	call [ebx+pCom_Printf]
	pop eax
	call [ebx+pCvar_Get]
	mov [ebx+cvTargServ],eax
	; add new cvar for fake server
	push 0
	lea eax,[ebx+hszNULStr]
	push eax
	lea eax,[ebx+hszNewCvar]
	push eax
	lea eax,[ebx+hszCvarFormat]
	push eax
	call [ebx+pCom_Printf]
	pop eax
	call [ebx+pCvar_Get]
	mov [ebx+cvFakeServ],eax
	add esp,14h
	pop dword ptr ds:[CON_ORMASK]	; 0 from cvar flags -> con.ormask
	lea eax,[ebx+hszRemember]
	push eax
	call [ebx+pCom_Printf]
	pop eax
cmds_done:
	push [ebx+dwHackKey]
	call thGetAsyncKeyState
	shr eax,1
	jnc notoggle
	mov eax,[ebx+bwallhack]
	add eax,1
	cmp al,2
	jna @F
	xor eax,eax
@@:	mov [ebx+bwallhack],eax
notoggle:
	push [ebx+dwAlphaKey]
	call thGetAsyncKeyState
	shr eax,1
	jnc noalphatoggle
	mov eax,[ebx+dwAlphaMode]
	mov ecx,GL_ONE_MINUS_SRC_ALPHA
	cmp eax,GL_ONE
	mov eax,GL_ONE
	cmovz eax,ecx
	mov [ebx+dwAlphaMode],eax
noalphatoggle:
	push [ebx+dwAlphaUpKey]
	call thGetAsyncKeyState
	fld [ebx+fltWallTrans]
	test ax,ax
	jns noalphaup
	fld1
	fcomip st(0),st(1)
	jb noalphaup				; clamp at 1.0
	fadd [ebx+fltWallInc]
	fst [ebx+fltWallTrans]
noalphaup:
	fstp st
	push [ebx+dwAlphaDnKey]
	call thGetAsyncKeyState
	test ax,ax
	jns noalphadn
	mov eax,[ebx+fltWallTrans]
	test eax,eax
	js noalphadn
	jz noalphadn
	fld [ebx+fltWallTrans]
	fsub [ebx+fltWallInc]
	fstp [ebx+fltWallTrans]
noalphadn:
	push [ebx+dwFbKey]
	call thGetAsyncKeyState
	shr eax,1
	jnc nofbtoggle
	xor [ebx+bFb],1
nofbtoggle:
	push [ebx+dwShKey]
	call thGetAsyncKeyState
	shr eax,1
	jnc noshtoggle
	fild dword ptr [ebx+dwShVal]
	fld dword ptr [ebx+fltMille]
	fmulp st(1),st
	push eax
	mov edx,3f800000h
	mov ecx,[ebx+cvTimeScale]
	mov eax,[ecx+14h]			; timescale->value
	cmp eax,edx
	fstp dword ptr [esp]
	; if zero? { val <- [esp] } else { val <- 1.0(edx) }
	pop eax				; put speedhack val into eax
	cmovnz eax,edx		; if val wasn't 1.0, write 1.0 (edx) into it
	mov [ecx+14h],eax	; do the actual write
noshtoggle:
	push [esp+8]				; first arg, but pass ebx
	call thwglSwapBuffers
	push eax					; save result
	push 3f800000h				; float 1.0
	push 0
	push 0
	push 0
	call thglClearColor			; -> glClearColor(0,0,0,1)
	push GL_COLOR_BUFFER_BIT
	call thglClear
	pop eax						; pop result of wglSwapBuffers
	pop ebx
	ret 4
noclear:
	push [esp+8]
	call thwglSwapBuffers
	pop ebx
	ret 4
; *** END wglSwapBuffers ***

COMMENT #
initxorgen proc uses ebx init:DWORD

	getdelta ebx
	xor eax,eax
	add eax,1
	mov ecx,init
	mov [ebx+v1],eax
	dec eax
	dec ecx
	jz init_one
	dec ecx
	jz init_two
	dec ecx
	dec ecx
	jz init_four
	mov [ebx+v0],259287D0h
	mov [ebx+v2],eax		; (eax is 0)
	mov [ebx+v3],7FFFFFFFh
	ret
init_one:
	mov [ebx+v0],41C64E6Dh
	mov [ebx+v2],00003039h
	mov [ebx+v3],80000000h
	ret
init_two:
	mov [ebx+v0],000041A7h
	mov [ebx+v2],eax		; (eax is 0)
	mov [ebx+v3],7FFFFFFFh
	ret
init_four:
	mov [ebx+v0],00010DCDh
	add eax,1
	mov [ebx+v2],eax		; (eax is 1)
	mov [ebx+v3],80000000h
	ret

initxorgen endp

; returns (v1  = (int)((unsigned)(v0*v1 + v2) % (unsigned)v3))
getxorbyte proc

	getdelta ecx
	xor edx,edx
	mov eax,[ecx+v0]
	imul eax,[ecx+v1]
	add eax,[ecx+v2]	; eax -> v0*v1 + v2
	div [ecx+v3]		; divide eax by v3
	mov [ecx+v1],edx	; v1 = remainder
	mov eax,edx
	ret

getxorbyte endp
#

xchap_encode proc C uses ebx esi edi szRStr:LPSTR, szServer:LPSTR, szName:LPSTR, szKey:LPSTR, szMap:LPSTR

	LOCAL dwLen:DWORD

	getdelta ebx
	; initialise pseudo-random sequence
;	invoke initxorgen,4
	xor eax,eax
	mov [ebx+v0],00010DCDh
	add eax,1
	mov [ebx+v2],eax		; (eax is 1)
	mov [ebx+v3],80000000h
	add eax,1
	mov [ebx+v1],eax
	; allocate buffer for temporary string
	push 100h
	push LPTR
	call thLocalAlloc
	mov esi,eax
	; NUL fifth byte of key
	mov eax,szKey
	mov byte ptr [eax+4],0
	; wsprintf(esi, "%s%s%02d%s%02d%s", key, "null", serverlen, server, namelen, name)
	push szName
	push szName
	call thlstrlen
	push eax
	mov eax,[ebx+cvFakeServ]
	mov eax,[eax+4]				; cvFakeServ->string
	mov cl,[eax]
	test cl,cl
	mov edx,szServer
	cmovz eax,edx
	push eax
	push eax
	call thlstrlen
	push eax
	lea eax,[ebx+szNull]
	push eax
	push szKey
	lea eax,[ebx+szFormat]
	push eax
	push esi
	call thwsprintfA
	add esp,20h				; clean up stack
	mov dwLen,eax
	xor ebx,ebx
	mov edi,szRStr
lstart:
;	invoke getxorbyte
	getdelta ecx
	xor edx,edx
	mov eax,[ecx+v0]
	imul eax,[ecx+v1]
	add eax,[ecx+v2]	; eax -> v0*v1 + v2
	div [ecx+v3]		; divide eax by v3
	mov [ecx+v1],edx	; v1 = remainder

	xor [esi],dl
	cmp ebx,3
	jbe belowthree
	xor edx,edx
	mov edx,ebx
	and edx,3
	mov ecx,szKey
	mov al,[ecx+edx]
	xor [esi],al
belowthree:
	movzx eax,byte ptr [esi]
	; undocumented aam operand
	db 0d4h, 26d	; ah <- al/26 , al <- al%26
	add ax,6161h
	mov [edi],ah
	add edi,1
	mov [edi],al
	add ebx,1
	add esi,1
	add edi,1
	cmp ebx,dwLen
	jb lstart

	mov eax,szRStr
	mov ecx,dwLen
	mov byte ptr [eax+ecx*2],0

	push esi
	call thLocalFree
	ret

xchap_encode endp

;xfake_chaptest:
;	getdelta edx
;	lea eax,[edx+szUK]
;	push eax
;	mov eax,[edx+pCom_Printf]
;	call eax
;	pop eax
;	ret

; int __cdecl CHAP_Decode(char *istr);
; can be overflowed by sending a big fake p_way response
; this protects clients while making them act like a real
; client
if PROTECT_CHDEC
xchap_decode:
	xor eax,eax
	or ecx,-1
	mov edx,[esp+4]
@@:	mov al,[edx]
	add ecx,1
	add edx,1
	test eax,eax
	jnz @B
	getdelta edx
	cmp ecx,512
	jb @nooverflow
	call @F
	db '[glhack] Somebody tried to crash you. Since a normal',0ah
	db '         q2ace client WOULD crash, we have to disconnect',0ah
	db '         at this point so as to avoid detection.',0ah,0
@@:	call [edx+pCom_Printf]
	add esp,4
	mov eax,FN_DISCONNECT
	call eax
	ret
@nooverflow:
	jmp [edx+pCHAP_Decode]
endif

; for the lame p_server thing
; gets passed stufftext
; int __cdecl CHAP_Stuff(char *stufftext);
; CHAP_Stuff returns:
; 1 for "dopversion"
; 2 for "dopmodified"
; 3 for "qnocmd"
; 4 for "qcmdlist"
; 5 for "cnocmd"
; 6 for "ccmdlist"
; 7 for "p_blocklist"
; 8 for "p_server"
xchap_stuff:
	push ebx
	push edi
	mov edi,[esp+0Ch]
	getdelta ebx
	push edi
if SHOWSTUFF
	lea eax,[ebx+hszStuffFmt]
	push eax
	call [ebx+pCom_Printf]
	pop ecx
endif
;	push edi
;	push esi
;	call @F
;@xcs_str:
;	db 'say Q2ADMIN_NOCMDCHECK '
;@@:	pop esi
;	db 6ah, ($ - @xcs_str - 2)	; force push byte
;	pop ecx
;	repz cmpsb
;	jmp @F
;	mov dword ptr [edi],'751'
;@@:	pop esi
;	pop edi
;	push edi
	call [ebx+pCHAP_Stuff]
	pop ecx
	cmp al,8		; 8 is p_server
	jnz @xcs_ret
;	jz @p_server
;	jmp @xcs_ret
@p_server:
	; now say a fake response and return any number > 8
	add edi,9		; edi -> argument
	or ecx,-1
	push edi
	xor eax,eax
	repnz scasb

	; end of arg
	lea edx,[edi-2]

	mov eax,[ebx+cvFakeServ]
	pop edi
	push edx
	mov edx,[eax+4]				; cvFakeServ->string
	push 8
	xor ecx,ecx
	mov cl,[edx]				; check 
	pop eax						; eax <- 8, then return
	test ecx,ecx
	pop ecx
	jz @xcs_ret
	mov byte ptr [ecx],0
	xchg eax,edx				; eax <- server
	sub esp,100h
	mov edx,esp
	push eax
	push edi
	call @F
	db 'q2ace v1.21 - SERVERIP %s %s',0ah,0
@@:
	push edx
	call thwsprintfA
	push [esp]
	mov eax,FN_GLOBALSAY
	call eax
	add esp,114h
@xcs_hret:
	or eax,-1
@xcs_ret:
	pop edi
	pop ebx
	ret

; fake p_auth, for unpatching/repatching
xchap_auth:
	push ebx
	getdelta ebx
	; FIXME: could be detected by checking page access
	push eax
	push esp
	push PAGE_EXECUTE_READWRITE
	push 59000h
	push 400000h
	call thVirtualProtect
	push esp
	push PAGE_EXECUTE_READWRITE
	push 4C000h
	push 30050000h
	call thVirtualProtect
	pop eax
	mov byte ptr ds:[P0],75h	; unpatch model check
;	mov dword ptr ds:[P1],P1o	; unpatch anti wallhack
;	mov dword ptr ds:[P2],P2o	; more anti wallhack
if NOROXTRAILS
	mov byte ptr ds:[P4],74h
endif
if NOCVARVALIDATE
	mov dword ptr ds:[P5],P5o
endif
if NOKICK
	mov dword ptr ds:[P6],P6o
endif
if TIMEAUTH
	call thGetTickCount
	push eax
	ex equ 8
else
	ex equ 4
endif
	push [esp+14h+ex]	; args and ebx
	push [esp+14h+ex]
	push [esp+14h+ex]
	push [esp+14h+ex]
	push [esp+14h+ex]
	call [ebx+pCHAP_UniCrypt]
	add esp,14h	; __cdecl
if TIMEAUTH
	pop ecx
	push eax
	push ecx
	call thGetTickCount
	sub eax,[esp]
	push eax
	call @F
	db '>> took %ums <<',0ah,0
@@: call [ebx+pCom_Printf]
	add esp,0ch
	pop eax
endif
	mov byte ptr ds:[P0],0ebh	; repatch model check
;	mov dword ptr ds:[P1],P1p
;	mov dword ptr ds:[P2],P2p
if NOROXTRAILS
	mov cl,[ebx+bRoxPatch]
	mov byte ptr ds:[P4],cl
endif
if NOCVARVALIDATE
	mov dword ptr ds:[P5],P5p
endif
if NOKICK
	mov ecx,[ebx+bKickPatch]
	mov dword ptr ds:[P6],ecx
endif
	pop ebx
	ret

xchap_crc:
xchap_asus:
	xor eax,eax
xchap_asusnew:
	ret

; hooked GetChapAPI
; as below
xGCA:
	push ebx
	getdelta ebx
	mov eax,[esp+14h]			; get Com_Printf from chapimport_t
	mov [ebx+pCom_Printf],eax
	mov eax,[esp+4]
	mov [ebx+dwRetTmp],eax
	lea eax,[ebx+gca_newret]
	mov [esp+4],eax
	mov eax,ebx
	pop ebx
	jmp [eax+rGCA]
gca_newret:
	getdelta edx
	mov eax,[edx+dwRetTmp]
	push eax						; push addr to return to
	mov eax,[esp+4]					; get addr of return struct
	lea ecx,[edx+xchap_test]
	mov [eax+4],ecx					; put fake debugace in struct
	; hook CHAP_UniCrypt
	lea ecx,[edx+xchap_auth]
	push [eax+8]
	mov [eax+8],ecx
	pop ecx
	mov [edx+pCHAP_UniCrypt],ecx
	; hook CHAP_Stuff
	lea ecx,[edx+xchap_stuff]
	push [eax+32]
	mov [eax+32],ecx
	pop ecx
	mov [edx+pCHAP_Stuff],ecx
if PROTECT_CHDEC
	; hook CHAP_Decode
	lea ecx,[edx+xchap_decode]
	push [eax+12]
	mov [eax+12],ecx
	pop ecx
	mov [edx+pCHAP_Decode],ecx
endif
	; replace CHAP_Encode
	lea ecx,[edx+xchap_encode]
	mov [eax+16],ecx				; put our CHAP_Encode in
	; replace CHAP_CRC, CHAP_Asus
	lea ecx,[edx+xchap_crc]
	mov [eax+20],ecx				; CHAP_CRC
	mov [eax+28],ecx				; CHAP_Asus
	ret

; hooked GetRefAPI
; impsize = 64d
; pm @96d, pc@104d size = 124d

;
; ###################
; # C struct thingy #
; ###################
;
; bigstruct_t func(otherstruct_t instr);
;
; push all_of_instr
; push pointer_to_where_i_want_bigstruct_t_to_go
; call func
;

xGRA:
	push ebx
	getdelta ebx
	mov eax,[esp+10h]			; pass ebx, retaddr, retptr to get Cmd_AddCommand pointer
	mov [ebx+pCmd_AddCommand],eax
	mov eax,[esp+14h]
	mov [ebx+pCmd_RemoveCommand],eax
	mov eax,[esp+34h]
	mov [ebx+pCvar_Get],eax
	mov eax,[esp+4]				; ret addr
	mov [ebx+dwRetTmp],eax		; save old ret addr
	lea eax,[ebx+changedret]	; change ret addr in the stack
	mov [esp+4],eax
	mov eax,ebx
	pop ebx
	jmp [eax+rGRA]
changedret:
	getdelta edx
	mov eax,[edx+dwRetTmp]
	push eax					; push real ret addr
	mov eax,[esp+4]
	lea ecx,[edx+x_r_init]
	push [eax+8]
	mov [eax+8],ecx				; hook R_Init
	pop [edx+pR_Init]			; save real R_Init
	lea ecx,[edx+xfake_pm]
	mov [eax+96],ecx			; stick fake p_modified in
	lea ecx,[edx+xfake_pc]
	mov [eax+104],ecx			; stick fake p_custom in
	ret

; hook R_Init and in the fake, alter commands after calling it
; so that screenshots are hooked after vid_restart
x_r_init:
 	push ebx
	getdelta ebx
	push eax
	push esp
	push PAGE_EXECUTE_READWRITE
	push 4
	push P0
	call thVirtualProtect
	test eax,eax
	pop eax
	jz @F
	mov eax,P0
	cmp byte ptr [eax],75h
	jnz @F
	; patch model resize
	mov byte ptr [eax],0ebh
if NOROXTRAILS
@@:	; patch rocket trails
	push eax
	push esp
	push PAGE_EXECUTE_READWRITE
	push 4
	push P4
	call thVirtualProtect
	test eax,eax
	pop eax
	jz @F
	mov al,[ebx+bRoxPatch]
	mov byte ptr ds:[P4],al
endif
@@:
if NOCVARVALIDATE
	; patch cvar thing
	push eax
	push esp
	push PAGE_EXECUTE_READWRITE
	push 4
	push P5
	call thVirtualProtect
	test eax,eax
	pop eax
	jz @F
	mov dword ptr ds:[P5],P5p
	@@:
endif
if NOKICK
	; patch kickangle adding
	push eax
	push esp
	push PAGE_EXECUTE_READWRITE
	push 4
	push P6
	call thVirtualProtect
	test eax,eax
	pop eax
	jz @F
	mov ecx,[ebx+bKickPatch]
	mov dword ptr ds:[P6],ecx
	@@:
endif
	push [esp+12]
	push [esp+12]
	call [ebx+pR_Init]
	push eax
	lea eax,[ebx+xscreenshotjpg]
	push eax
	lea eax,[ebx+hszScreenshotjpg]
	push eax
	call [ebx+pCmd_RemoveCommand]	; an advantage of __cdecl !
	call [ebx+pCmd_AddCommand]
	lea eax,[ebx+xscreenshot]
	push eax
	lea eax,[ebx+hszScreenshot]
	push eax
	call [ebx+pCmd_RemoveCommand]
	call [ebx+pCmd_AddCommand]
	add esp,24
	pop eax
	pop ebx
	ret

; fake p_modified and p_custom
xfake_pm:
	getdelta eax
	lea eax,[eax+szModelsOK]
	ret

xfake_pc:
	getdelta eax
	lea eax,[eax+szCustomOK]
	ret

; speedup some of mike's crap code :)
xSetFileAttributesA:
xEnumWindows:
	xor eax,eax
	add eax,1
	ret 8

xFindNextFileA:
	xor eax,eax
	ret 8

; hook functions
newGPA:
	push edi
	push ebx
	getdelta edi
	mov ebx,[esp+8+8] ; pass edi, ebx, retaddr, hModule to get szFuncName
	cmp ebx,0ffffh
	jbe ordinal
	push ebx
	call @F
	db 'GetRefAPI',0
@@:	call thlstrcmp
	test eax,eax
	jnz notGRA
	push ebx
	push [esp+4+8+4] ; pass ebx, (edi and ebx), retaddr to get hModule
	call thGetProcAddress
	mov [edi+rGRA],eax
	db 0b8h
fFB	dd (xGRA - HookCode)
	jmp hookret
notGRA:
	push ebx
	call @F
	db 'GetChapAPI',0
@@:	call thlstrcmp
	test eax,eax
	jnz notGCA
	push ebx
	push [esp+4+8+4]
	call thGetProcAddress
	mov [edi+rGCA],eax
	db 0b8h
fFC	dd (xGCA - HookCode)
	jmp hookret
notGCA:
	hookfn glBegin
	hookfn glEnd
	hookfn glIsEnabled
	hookfn glVertex3fv
	hookfn glColor4f
	hookfn glReadPixels
	; no macro for this as it has a different jump
	push ebx
	call @F
	db 'wglSwapBuffers',0
@@:	call thlstrcmp
	test eax,eax
	jnz ordinal
	db 0b8h
fFD	dd (xwglSwapBuffers - HookCode)
hookret:
	pop ebx
	pop edi
	ret 8
ordinal:
	pop ebx
	pop edi
	jmp thGetProcAddress

xLoadLibrary:
	push ebx
	push [esp+4+4]
	call thLoadLibraryA
	test eax,eax
	jz @llret
	js @llret				; don't patch IAT of system dlls

	push eax

	;
	push (newGPA-HookCode)
fixup F002
	call @F
	db 'GetProcAddress',0
@@:	call @F
	db 'KERNEL32.DLL',0
@@:	mov ebx,[esp]
	push eax
	call HackIAT
	;

	;
	mov eax,[esp]
	push (xLoadLibrary-HookCode)
fixup F003
	call @F
	db 'LoadLibraryA',0
@@:	push ebx
	push eax
	call HackIAT
	;
if FASTAUTH
	;
	mov eax,[esp]
	push (xSetFileAttributesA-HookCode)
fixup F004
	push [esp]		; save it for EnumWindows and avoid another fixup!
	call @F
	db 'SetFileAttributesA',0
@@:	push ebx
	push eax
	call HackIAT
	;

	; addr of xEnumWindows (==xSetFileAttributes) already on stack
	mov eax,[esp+4]
	call @F
	db 'EnumWindows',0
@@:	call @F
	db 'USER32.DLL',0
@@:	push eax
	call HackIAT
	;

	;
	mov eax,[esp]
	push (xFindNextFileA-HookCode)
fixup F005
	call @F
	db 'FindNextFileA',0
@@:	push ebx
	push eax
	call HackIAT
	;
endif
	pop eax
@llret:
	pop ebx
	ret 4

HackIAT proc uses esi edi ebx lpBase:LPVOID, szDll:LPSTR, szFn:LPSTR, lpNewFn:DWORD

	LOCAL dwTMP:DWORD
	mov esi,lpBase
	cmp word ptr [esi],IMAGE_DOS_SIGNATURE
	jnz exit
	add esi,[esi+3Ch]						; esi = IMAGE_NT_HEADERS
	cmp dword ptr [esi],IMAGE_NT_SIGNATURE
	jnz exit
	mov ebx,lpBase
	add ebx,[esi+80h]						; ebx = IMAGE_IMPORT_DESCRIPTOR
	assume ebx: PTR IMAGE_IMPORT_DESCRIPTOR
findloop:
	mov eax,[ebx].FirstThunk
	test eax,eax							; while FirstThunk
	jz exit
	mov edi,lpBase
	add edi,[ebx].Name1
	;
	push szDll
	push edi
	call thlstrcmpi							; is this the target dll?
	;
	test eax,eax
	jnz nomatch
	mov ecx,lpBase
	mov edx,ecx
	add ecx,[ebx].FirstThunk
	add edx,[ebx].OriginalFirstThunk
prodloop:
	assume ecx: PTR IMAGE_THUNK_DATA
	assume edx: PTR IMAGE_THUNK_DATA
	mov eax,[ecx].u1.Function
	test eax,eax							; any more functions?
	jz exit
	mov eax,[edx].u1.Ordinal
	test eax,eax
	js skipfn								; skip if imported by ordinal
	mov eax,[edx].u1.AddressOfData
	add eax,2								; skip hint
	add eax,lpBase							; eax = function name
	pushad
	;
	push szFn
	push eax
	call thlstrcmp
	;
	test eax,eax
	popad
	jnz nofmatch
	mov eax,lpNewFn
	push edx
	push ecx
	push eax
	;
	lea eax,dwTMP
	push eax
	push PAGE_EXECUTE_READWRITE
	push 4
	push ecx
	call thVirtualProtect
	;
	test eax,eax
	pop eax
	pop ecx
	jz @badptr	; avoid page faults
	mov [ecx].u1.Function,eax
	push ecx
	push ebx
	mov ebx,dwTMP
	;
	lea eax,dwTMP
	push eax
	push ebx
	push 4
	push ecx
	call thVirtualProtect
	;
	pop ebx
	pop ecx
@badptr:
	pop edx
nofmatch:
skipfn:
	add ecx,sizeof(IMAGE_THUNK_DATA)
	add edx,sizeof(IMAGE_THUNK_DATA)
	jmp prodloop
nomatch:
	add ebx,sizeof(IMAGE_IMPORT_DESCRIPTOR)	; go to next dll
	jmp findloop
exit:
	assume ebx:nothing
	assume ecx:nothing
	assume edx:nothing
	ret

HackIAT endp

HookCodeLen equ $ - HookCode

; >>>>>>>>>>> INJECTION CODE <<<<<<<<<<<<
InjCodeStart:
	pushad
	push (newGPA-HookCode)
fixup H00
	call @F
	db 'GetProcAddress',0
@@:
	call @F
	db 'KERNEL32.DLL',0
@@:
	pop edi
	push edi
	mov eax,00400000h
	mov ebp,eax
	push eax
	mov eax,(HackIAT-HookCode)
fixup H01
	mov ebx,eax
	call eax
	push (xLoadLibrary-HookCode)
fixup H02
	call @F
	db 'LoadLibraryA',0
@@:	push edi
	push ebp
	call ebx
	popad
	ret
InjCodeEnd:
InjCodeLen equ (InjCodeEnd - InjCodeStart)
; <<<<<<<<<<< INJECTION CODE >>>>>>>>>>>>
