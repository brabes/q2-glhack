
## g l h a c k
: OpenGL wallhack for Quake 2 (specifically q2ace)

### Overview

This is a cheat for Quake 2, it only works for the specific version of
Q2ACE that it targets. Unfortunately I did not keep old versions of my
source code so this is the only version available (although I do have
old binaries).

**NOTE** the binary is often falsely flagged by antivirus programs;
this is because of the coding methods used (e.g. self-modifying code,
anti-debug methods) and is a heuristic check by the AV, rather than
an actual detection.

### How it works

As Q2ACE primarily works by checking the names of loaded modules, this
hack allocates memory directly within the Q2ACE process. It then writes
its hook code there, manually applies relocations and then runs it to
patch the IAT (import address table) of the Q2ACE modules. This hooks
the OpenGL functions to apply a wallhack. It also patches certain parts
of Q2ACE to bypass checks and 

### Features

* Wallhack, three modes
* Workaround for server forced reconnection, allowing Ratbot to be used
* Speedhack (detectable serverside)
* Fullbright mode
* Disables cheats for screenshots
* Disables Q2ACE model resizing
* Disable kickangles
* Disable rocket trails
* Protects from buffer overflow in Q2ACE
* Speeds up Q2ACE greatly by bypassing its broken checks

### History of Q2ACE

#### Origins

In the noughties people still played Quake 2 online, and after John Carmack
released the source code for Quake 2, people made anticheat addons for it.

A notable one was 'q2ace' (standing for 'anti-cheat edition') which came with
a dll that, if present, added functionality to the Quake 2 client in that it
would react to certain messages sent via the chat in order to ensure that no
proxy was being used, and no DLL hooks were being used. Ordinarily this would
be questionable with the GPL that the Quake 2 source code was released under,
but it came with a specific exception for this purpose.

#### Methods

It appeared to have been written in response to the ASUS drivers that
included a wallhack mode, as its primary functionality revolved around
getting the list of DLLs loaded into it using the Win32 API
`CreateToolhelp32Snapshot` and comparing the module names to a
predetermined list (e.g. the ASUS modules were called `anvioctl`, `v66osd`
and `anvosd`). Unfortunately it had some very unspecific names such as
`hook.dll`, which meant that people using innocent programs were often
flagged; in particular there was a popular chat program at the time that
created a DLL with this name and was therefore 'detected'. This cheat,
by contrast, wrote itself directly into the Q2ACE process's memory,
meaning the module list was untouched. I'm not sure if the Q2ACE
maintainer ever realized this.

There was also a cheat at the time that locked the mouse onto specific
colours that displayed on the screen, which it did without directly
interacting with whichever game was running (Quake 2 in this case). That
meant that by using, for example, bright red skins for enemies, this
program was a crude aimbot. In order to 'detect' this, q2ace went through
all the open windows with `EnumWindows` and checked the titles for
forbidden substrings (yes, really). That meant that, for example, having
the 'OGC' (online gaming cheats, a popular cheats website at the time)
website open while playing a game (or indeed any other window with those
three letters in its title), would result in another 'detection'.

One of the main features of Q2ACE was its `p_auth` functionality; when
this message was received it would return the result of a series of
calls to `rand()` spread throughout the code, combined with a function
of the checksum of the Q2ACE modules in memory. All legitimate clients
would return the same value, and so if there were enough players on
a server, any anomalies would stand out. This meant a bad client
could emulate q2ace by copying the most popular response and then
printing it out.

There was also a function to prove that a proxy had not been used, this
worked by 'encrypting' the user's name and server, then printing this
out. Clients that received the message would decrypt the message to
print out what server they were using; this message was unique to each
user at the time and could therefore not simply be copied. However the
'encryption' code was fairly basic and was easily reverse engineered.

The strings used by the authentication module are encrypted and appear
as strings of numbers within the DLL. This makes reverse engineering
a little less straightforward, however dumping the strings is fairly
trivial.

#### Customer relations

Q2ACE ran a support forum on the 'Savage Forums', where the maintainer
and his cronies would berate and abuse anyone who had issues with the
program, using foul language and refusing to fix any bugs or simply
denying their existence. They often boasted that their anticheat would
be able to defeat any publicly released hack, with one of the promoters
known as "Mr^B" (real name Lee Barnard) stating:

> If they release it we can get it and we can then counter it

As time went on Q2ACE became more bloated and began to add broken 'features',
such as 'detecting' oversized models (a cheat at the time was to use models
with large beams protruding along each axis as a makeshift wallhack), which
also flagged anyone playing the Action Quake 2 mod as a cheater.

They also added a scan of all files in the Quake 2 directory, as a result of
this hack, and would alter responses based on this. A consequence of this was
that to produce its authentication code would take around 600ms, during which
time the game would be frozen.

At one point they added checks to avoid drawing models that should be hidden
by the map, however this caused significant framerate drops and would either
hide models that should have been visible, or only hide very distant models.

Each time that their users had trouble with these anti-features, they would be
abused and dismissed, and often banned from the support forums.

### Attempts at countering the hack

The first attempt involved checking the value of `GL_DEPTH_TEST` around calls
to `glBegin()`. However, this was easily defeated by hooking `glBegin()`.

As early versions of the hack disabled the `debugace` command, which the
developers used to get a list of loaded modules, this was used as a 'detection'
method by some admins.

Q2ACE did not originally include its own memory space as part of the generation
of its `p_auth` response, this was added later but easily defeated by hooking
the function and unpatching, then repatching once it was complete.

The Q2ACE developers also made an illegally modified version of q2admin, which
had been released under GPLv2, with added features to integrate with Q2ACE.
Because this cheat adds an extra command to get around forced reconnection that
was used by some servers to prevent proxies being used, there was a 'secret'
functionality where Q2ACE would send the modified q2admin the number of commands
available. This never seemed to actually work, but was easily hooked in any case.

The final, desperate attempt was in Q2ACE v1.20, where the modules were packed
with tElock. As this uses IAT redirection and scrambling, it confounded the
method by which this hack inserted itself. However, the method was changed
to patching the EAT of hooked modules. This packer also caused issues for
a number of users, including false virus detections and frequent crashes; as
usual, users bringing this to the teams attention were banned and berated. For
the final version of Q2ACE they backed out of this and supplied uncompressed
modules.

At one point Lee Barnard aka "Mr^B" sent me an email accusing me of ruining
Quake 2; when I replied that it was in response to their mistreatment of
users he smugly replied:

> Thanks for that, it's been a pleasure dealing with you

...it turned out that their plan was to find my IP address in the email headers
from the reply and scour their logs to expose me. Unfortunately for them I only
ever played using the same handle I released the cheat under and they gained
nothing. Presumably they thought I was some prominent player and were planning
a grand exposé, but they were disappointed.

In every case a new version of glhack was released the same day as the updated
q2ace, despite their delaying source code release, and modifying the
sourcecode prior to release. After version 1.21 of their program, and 2 months
after the glhack was first released, they gave up on their product. Meanwhile
another anticheat called 'nocheat' was released, by a much more pleasant
developer, and all was well.
