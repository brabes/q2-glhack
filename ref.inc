
; ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒

refexport_t STRUCT
	DWORD waste_space
	DWORD api_version
	DWORD fn_Init
	DWORD fn_Shutdown
	DWORD fn_BeginRegistration
	DWORD fn_RegisterModel
	DWORD fn_RegisterSkin
	DWORD fn_RegisterPic
	DWORD fn_SetSky
	DWORD fn_EndRegistration
	DWORD fn_RenderFrame
	DWORD fn_DrawGetPicSize
	DWORD fn_DrawPic
	DWORD fn_DrawStretchPic
	DWORD fn_DrawChar
	DWORD fn_DrawCharSized
	DWORD fn_DrawTileClear
	DWORD fn_DrawFill
	DWORD fn_DrawFadeScreen
	DWORD fn_DrawStretchRaw
	DWORD fn_CinematicSetPalette
	DWORD fn_BeginFrame
	DWORD fn_EndFrame
	DWORD fn_AppActivate
	DWORD fn_p_modified
	DWORD fn_do_asus_check
	DWORD fn_p_custom
	DWORD fn_initiate_asus_check
	DWORD fn_stop_graphics_check
	DWORD fn_graphics_check
refexport_t ENDS

; ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒

refimport_t STRUCT
	DWORD fn_Sys_Error
	DWORD fn_Cmd_AddCommand
	DWORD fn_Cmd_RemoveCommand
	DWORD fn_Cmd_Argc
	DWORD fn_Cmd_Argv
	DWORD fn_Cmd_ExecuteText
	DWORD fn_Con_Printf
	DWORD fn_FS_LoadFile
	DWORD fn_FS_FreeFile
	DWORD fn_FS_Gamedir
	DWORD fn_Cvar_Get
	DWORD fn_Cvar_Set
	DWORD fn_Cvar_SetValue
	DWORD fn_Vid_GetModeInfo
	DWORD fn_Vid_MenuInit
	DWORD fn_Vid_NewWindow
refimport_t ENDS

; ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒

