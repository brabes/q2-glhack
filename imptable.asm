
; fn pointers
pLoadLibraryA dd 0
pGetProcAddress dd 0
pGetLastError dd 0
pLocalAlloc dd 0
pLocalFree dd 0
plstrlen dd 0
plstrcmpi dd 0
plstrcmp dd 0
pVirtualProtect dd 0
pCreateThread dd 0
pGetTickCount dd 0

pglBegin dd 0
pglEnd dd 0
pglIsEnabled dd 0
pglEnable dd 0
pglDisable dd 0
pglColor4f dd 0
pglVertex3fv dd 0
pglClearColor dd 0
pglClear dd 0
pwglSwapBuffers dd 0
pglReadBuffer dd 0
pglReadPixels dd 0
pglGetFloatv dd 0
pglBlendFunc dd 0
;pglShadeModel dd 0

pGetAsyncKeyState dd 0
pwsprintfA dd 0

pgethostbyname dd 0

jmp_table_offset equ $ - HookCode
thunk_count equ jmp_table_offset / 4

; jmp table
jmp_table:

; kernel32 functions
jmptab LoadLibraryA
jmptab GetProcAddress
jmptab GetLastError
jmptab LocalAlloc
jmptab LocalFree
jmptab lstrlen
jmptab lstrcmpi
jmptab lstrcmp
jmptab VirtualProtect
jmptab CreateThread
jmptab GetTickCount

; opengl32 functions
jmptab glBegin
jmptab glEnd
jmptab glIsEnabled
jmptab glEnable
jmptab glDisable
jmptab glColor4f
jmptab glVertex3fv
jmptab glClearColor
jmptab glClear
jmptab wglSwapBuffers
jmptab glReadBuffer
jmptab glReadPixels
jmptab glGetFloatv
jmptab glBlendFunc
;jmptab glShadeModel

; user32 functions
jmptab GetAsyncKeyState
jmptab wsprintfA

; wsock32 functions
jmptab gethostbyname

; end of jmp table
