
GetProcAddr		PROTO	:DWORD,:LPSTR
GetKernelBase	PROTO	:DWORD

; impfn
impfn MACRO fnname:REQ
	push offset sz&fnname
	push ebx
	call GetProcAddr
	mov _&fnname,eax
endm

; ------ STRUCTS ------
sSEH STRUCT
	OrgEsp            DD ?
	OrgEbp            DD ?
	SaveEip           DD ?
sSEH ENDS

.const
MAX_API_STRING_LENGTH	equ	150
MIN_KERNEL_SEARCH_BASE	equ 70000000h

.data?

SEH sSEH<?>
dwK32Base dd ?
_CloseHandle dd ?
_CreateProcessA dd ?
_CreateRemoteThread dd ?
_ExitProcess dd ?
_GetExitCodeThread dd ?
_LoadLibraryA dd ?
_ResumeThread dd ?
_Sleep dd ?
_VirtualAllocEx dd ?
_VirtualFreeEx dd ?
_WaitForSingleObject dd ?
_WriteProcessMemory dd ?


